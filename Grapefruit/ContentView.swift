//
//  ContentView.swift
//  Grapefruit
//
//  Created by Михаил on 31.01.2020.
//  Copyright © 2020 Михаил. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        Text("Hello, World!")
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
