//
//  Loop player.swift
//  Grapefruit WatchKit Extension
//
//  Created by Михаил on 22.05.2020.
//  Copyright © 2020 Михаил. All rights reserved.
//

import SwiftUI
import WatchKit



//class HostingController: WKHostingController<PlayerLoopVideos> {
//
//    override var body: PlayerLoopVideos {
//        return PlayerLoopVideos(video: WKInterfaceMovie)
//    }
//}

struct WatchVideoView: WKInterfaceObjectRepresentable {
    func makeWKInterfaceObject(context: WKInterfaceObjectRepresentableContext<WatchVideoView>) -> WKInterfaceMovie {
        return WKInterfaceMovie()
    }
    
    typealias WKInterfaceObjectType = WKInterfaceMovie
    
    
    var video = Bundle.main.url(forAuxiliaryExecutable: "Gif.gif.mp4")
    
    
    func updateWKInterfaceObject(_ movie: WKInterfaceMovie, context: WKInterfaceObjectRepresentableContext<WatchVideoView>) {
        movie.setMovieURL(video!)
        movie.setVideoGravity(.resizeAspect)
        
    }
}

//class PlayerLoopVideos : WKInterfaceController {
//    override init(frame: CGRect) {
//        super.init(frame: frame)
//    }
//
//    required init?(coder: NSCoder) {
//        fatalError("init(coder:) havent been implemented")
//    }
//}
//
struct Loop_player: View {
    var body: some View {
        WatchVideoView()
    }
}

struct Loop_player_Previews: PreviewProvider {
    static var previews: some View {
        Loop_player()
    }
}
