//
//  TechniquesScrollView.swift
//  Grapefruit WatchKit Extension
//
//  Created by Михаил on 31.01.2020.
//  Copyright © 2020 Михаил. All rights reserved.
//

import SwiftUI
import Combine

struct TechniquesList: View {
    
   @ObservedObject var model: TechniquesModel

    var body: some View {
        VStack(alignment: .leading) {
            Rectangle()
                .frame(width: 20.0, height: 15.0)
                .foregroundColor(Color.black)
            List {
                            ForEach(model.techniques) { technique in
                                NavigationLink(destination: InstructionList(technique: technique)) {
                                    TechniquesCell(technique: technique)
                                        .frame(height: 100.0)
                                        .shadow(radius: 20)
                                    
                                }
                                .listRowPlatterColor(Color(technique.color))
                            }
                            .onMove { self.model.moveTechnique(from: $0, to: $1) }
//                            .onDelete { self.model.deleteTechnique(at: $0) }
                        }
                        .listStyle(CarouselListStyle())
                        .navigationBarTitle(Text("Grapefruit"))
        }
            
        }
    }

struct TechniquesCell: View {
    var technique: Technique

    var body: some View {
        HStack {
            VStack(alignment: .leading) {
                Text(technique.title)
                    .font(.system(size: 23, design: .rounded))
                    .fontWeight(.semibold)
                    Spacer()
                
                Text(technique.description)
                    .font(.body)
                    .fontWeight(.light)
            }
        }
    .padding()
    }
}


struct TechniquesList_Previews: PreviewProvider {
    static var previews: some View {
        TechniquesList(model: TechniquesModel())
    }
}
