//
//  Techniques.swift
//  Grapefruit
//
//  Created by Михаил on 31.01.2020.
//  Copyright © 2020 Михаил. All rights reserved.
//

import UIKit
import SwiftUI
import Combine

//extension Int: Identifiable {
//    public var id: Int {
//        self
//    }
//}


struct Technique: Identifiable {
    let id = UUID()
    let title: String
    let color: UIColor
    let description: String
    let instructions: String
}

class TechniquesModel: ObservableObject {
    
    @Published var techniques:  [Technique] = [
        Technique(title: "Edging",
                  color:  UIColor(red:0.9, green:0.2, blue:0.19, alpha:1.0),
                  description: "To zero and rebuilding",
                  instructions: "With the pausing method, you completely stop all touch just before orgasm. When the feeling of impending orgasm is completely gone, you start over again from the beginning."
                ),
        Technique(title: "Accenting",
                  color: UIColor(red:0.9, green:0.2, blue:0.19, alpha:1.0),
                  description: "Searching for the best",
                  instructions: "Take the repeating motions that you enjoy the most and try to accent one zone for a while and see how that feels. Then give a try to other zones."
                ),
        Technique(title: "Rhythm",
                  color:  UIColor(red:0.9, green:0.2, blue:0.19, alpha:1.0),
                  description: "Follow your body",
                  instructions: "Repeat your motion right after finishing the last one with no pauses between. With this rhythm, there’s no time for pleasure to die down between motions."
                ),
        Technique(title: "Unexpected",
          color:  UIColor(red:0.9, green:0.2, blue:0.19, alpha:1.0),
          description: "Exiting suspense",
          instructions: "When each motion is different to the last. Try taking the motions that already work for you and vary each repetition. The variation can be in location or in rhythm."
        ),
        Technique(title: "Skipping",
          color:  UIColor(red:0.9, green:0.2, blue:0.19, alpha:1.0),
          description: "Almost a blur",
          instructions: "You take a break in between each motion or every other motion. The best way to try is to take a touch that already works for you and skip part of it, over and over."
        ),
        Technique(title: "Pulsating",
          color:  UIColor(red:0.9, green:0.2, blue:0.19, alpha:1.0),
          description: "Pleasing skip",
          instructions: "It is so fast that it’s almost a blur. You move so quickly that it becomes a flutter. It allows a tiny amount of movement to constantly stimulate yourself."
        )
    ]
    
    
    func moveTechnique(from source: IndexSet, to destination: Int) {
        guard let index = source.first else {
            return
        }
        let element = techniques.remove(at: index)
        techniques.insert(element, at: destination)
    }
    
//    func deleteTechnique(at indices: IndexSet) {
//        indices.forEach { techniques.remove(at: $0) }
//    }
}

extension Technique {
    static let previewTechnique = Technique(title: "Edging", color: .white, description: "blah blah", instructions: "blah blah")
}

