//
//  Start_Pause_Stop.swift
//  Grapefruit WatchKit Extension
//
//  Created by Михаил on 01.02.2020.
//  Copyright © 2020 Михаил. All rights reserved.
//

import SwiftUI

struct Start_Stop_Screen : View {
    
    @State var selection: Int? = nil
    @State var selection1: Int? = nil

    
    var body: some View {
        VStack {
            NavigationLink(destination: Animation_Screen(), tag: 1, selection: $selection) {
                Button(action: {self.selection = 1}) {
                    Text("Continue")
                }
            }
            NavigationLink(destination: TechniquesList(model: TechniquesModel()), tag: 1, selection: $selection1) {
                Button(action: {self.selection1 = 1}) {
                Text("Finish")
                }
            }
        }
    }
}

struct Start_Stop_Screen_Previews: PreviewProvider {
    static var previews: some View {
        Start_Stop_Screen()

    }
}
