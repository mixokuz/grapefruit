//
//  Animation.swift
//  Grapefruit WatchKit Extension
//
//  Created by Михаил on 31.01.2020.
//  Copyright © 2020 Михаил. All rights reserved.
//

import UIKit
import SwiftUI
import Combine

struct Animation_Appearance: View {
    
    @State var moveOn : Bool = false
    @State var colorBlack:Color = .black
    @State var selection: Int? = nil
    
    var body: some View {
        VStack {
            ZStack {
                
                
//                        Rectangle()
//                            .fill(colorBlack)
                        Dot()
                            .opacity(moveOn ? 1 : 0.33)
                            .scaleEffect(moveOn ? 1 : 2)
                            .animation(Animation.linear(duration: 1).repeatForever(autoreverses: true))
                            .offset(x: moveOn ? 30 : 0, y: moveOn ? 30 : 0)
                            .onAppear(){self.moveOn.toggle()}
                        
                        Dot()
                            .opacity(moveOn ? 1 : 0.33)
                            .scaleEffect(moveOn ? 1 : 2)
                            .animation(Animation.linear(duration: 1).repeatForever(autoreverses: true))
                            .offset(x: moveOn ? 0 : 0, y: moveOn ? -20 : 0)
                            .onAppear(){self.moveOn.toggle()}
                        
                        Dot()
                            .opacity(moveOn ? 1 : 0.33)
                            .scaleEffect(moveOn ? 1 : 2)
                            .animation(Animation.linear(duration: 1).repeatForever(autoreverses: true))
                            .offset(x: moveOn ? -30 : 0, y: moveOn ? 20 : 0)
                            .onAppear(){self.moveOn.toggle()}
                        
                    }
                .frame(width: 160, height: 100)
//            NavigationLink(destination: Start_Stop_Screen(), tag: 1, selection: $selection) {
//                Button(action: {self.selection = 1}) {
//                    Text("Pause")
//                }
//            }
        }
        
    }
}

struct Animation_Appearance_Previews: PreviewProvider {
    static var previews: some View {
        Animation_Appearance()
    }
}


