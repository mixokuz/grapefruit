//
//  File.swift
//  Grapefruit WatchKit Extension
//
//  Created by Михаил on 31.01.2020.
//  Copyright © 2020 Михаил. All rights reserved.
//

import SwiftUI

struct Animation_Screen: View {
    var body: some View {
        VStack {
            Animation_Appearance()
        }
    }
}

struct Animation_Screen_Previews: PreviewProvider {
    static var previews: some View {
        Animation_Screen()

    }
}
