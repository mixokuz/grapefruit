//
//  test.swift
//  Grapefruit WatchKit Extension
//
//  Created by Михаил on 31.01.2020.
//  Copyright © 2020 Михаил. All rights reserved.
//

import SwiftUI

struct test: View {
    
    @EnvironmentObject var data: TechniquesModel
    
    var body: some View {
        Text("\(data.techniques[0].description)")
    }
}

struct test_Previews: PreviewProvider {
    static var previews: some View {
        test()
        .environmentObject(TechniquesModel())
    }
}
