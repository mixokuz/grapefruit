//
//  InstructionScreen.swift
//  Grapefruit WatchKit Extension
//
//  Created by Михаил on 31.01.2020.
//  Copyright © 2020 Михаил. All rights reserved.
//

import SwiftUI

struct InstructionList: View {
    
    @EnvironmentObject var techniquesModel : TechniquesModel
    var technique : Technique
    
//    @State var selectedTechnique : Int = 0
    @State var colorGrapefruit:Color = Color(UIColor(red:0.9, green:0.2, blue:0.19, alpha:1.0))
    @State var colorBlack:Color = .black
    @State var selection: Int? = nil

    
    var body: some View {
        
            VStack {
                ScrollView (.vertical) {
                        Text(technique.title)
                            .multilineTextAlignment(.leading)
                            .foregroundColor(colorGrapefruit)
                            .font(.system(size: 27, weight: .semibold, design: .rounded))
                            .padding(.top, 65.0)
                            .frame(width: 160, height: 50, alignment: .leading)
                        Divider()
                        Spacer()
                        ZStack {
                            Rectangle()
                            .fill(colorBlack)
                            VStack {
                                Text(technique.instructions)
                                    .multilineTextAlignment(.leading)
                                    .frame(width: 160, height: 200, alignment: .leading)
//                                Rectangle()
//                                    .fill(colorBlack)
                            }
                        }
            //            NavigationLink(destinationName: AnimationController(), label: "AnimationControllerLabel")
                        NavigationLink(destination: Animation_Screen(), tag: 1, selection: $selection) {
                            Button(action: {self.selection = 1}) {
                                HStack {
                                    Spacer()
                                    Text("Start session")
                                        .foregroundColor(Color.white).bold()
                                    Spacer()
                                }
                            }
                        }
                        .buttonStyle(PlainButtonStyle())
                    }
                .padding()
        }
        
    }
}

struct InstructionList_Previews: PreviewProvider {
    static var previews: some View {
        InstructionList(technique: Technique.previewTechnique)

    }
}
