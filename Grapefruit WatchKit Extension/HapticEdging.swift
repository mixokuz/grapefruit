//
//  HapticEdging.swift
//  Grapefruit WatchKit Extension
//
//  Created by Михаил on 31.01.2020.
//  Copyright © 2020 Михаил. All rights reserved.
//

import SwiftUI

struct Edging: View {
    let timer = Timer.publish(every: 1, on: .main, in: .common).autoconnect()
    @State private var sliderVal = 0.0
    @State private var grape = false
    @State var firstTimeIn = true
    private var width: CGFloat = 30.0
    private var height: CGFloat = 60.0
    
    var body: some View {
        VStack {
            // --- Starting shapes construction and animation ---
            ZStack {
                   RadialGradient(gradient: Gradient(colors: [Color.black, Color.black]), center: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/, startRadius: /*@START_MENU_TOKEN@*/5/*@END_MENU_TOKEN@*/, endRadius: /*@START_MENU_TOKEN@*/500/*@END_MENU_TOKEN@*/)
                   .scaleEffect(1.2)
                   ZStack {
                       ZStack {
                           ZStack {
                               RadialGradient(gradient: Gradient(colors: [Color.pink, Color.red]), center: .center, startRadius: 5, endRadius: 100)
                                   .clipShape(Circle()).frame(width: width, height: height)
                                   .offset(y: -41)
                           }

                       }.opacity(0.5)

                       ZStack {

                           ZStack {
                               RadialGradient(gradient: Gradient(colors: [Color.pink, Color.red]), center: .center, startRadius: 5, endRadius: 90)
                                   .clipShape(Circle()).frame(width: width, height: height)
                                   .offset(y: 41)
                           }
                       }.opacity(0.5).rotationEffect(.degrees(60), anchor: .center)

                       ZStack {
                           ZStack {
                               RadialGradient(gradient: Gradient(colors: [Color.pink, Color.red]), center: .center, startRadius: 5, endRadius: 100)
                                   .clipShape(Circle()).frame(width: width, height: height)
                                   .offset(y: -41)
                           }

                       }.opacity(0.5).rotationEffect(.degrees(60*2), anchor: .center)

                       }
                       .rotationEffect(.degrees(grape ? 360 : 0), anchor: .center)
                       .scaleEffect(grape ? 1 : 0.2)
                       .animation(Animation.easeInOut(duration: 4).delay(1).repeatForever(autoreverses: true))
                           .opacity(grape ? 1 : 0.5)
                   }
            // --- Endding shapes construction and animation ---
            
            Text("Slider val: \(sliderVal)")
            Slider(value: $sliderVal)
            }
            .onReceive(timer) { input in
                let percentage = self.sliderVal * 100
                
                if (percentage > 0 && percentage <= 30 ) {
                    self.firstTimeIn = false
                    self.grape.toggle()
                    WKInterfaceDevice.current().play(.success) // Haptic low
                } else if (percentage > 30 && percentage <= 60 ) {
                    WKInterfaceDevice.current().play(.failure) // Haptic medium
                } else if (percentage > 60 && percentage <= 100 ) {
                    WKInterfaceDevice.current().play(.notification) // Haptic strong
                } else if (percentage == 0 && !self.firstTimeIn) {
                    self.grape.toggle()
                }
        }
    }
}

struct Edging_Previews: PreviewProvider {
    static var previews: some View {
        Edging()
    }
}
