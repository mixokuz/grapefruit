//
//  Dot.swift
//  Grapefruit WatchKit Extension
//
//  Created by Михаил on 31.01.2020.
//  Copyright © 2020 Михаил. All rights reserved.
//

import SwiftUI

struct Dot: View {
    
    var dot_center = Color(red: 209, green: 78, blue: 57)
    var dot_o = Color(red: 230, green: 168, blue: 152)
    var dot_edge = Color(red: 245, green: 214, blue: 199)
    var dot_image = #imageLiteral(resourceName: "D14E39_")
    
    var body: some View {
        ZStack {
            Image("D14E39_")
                .scaleEffect(0.05)
        }
    }
    
    init() {
        
    }
}

struct Dot_Preview: PreviewProvider {
    static var previews: some View {
        Dot()
    }
}
