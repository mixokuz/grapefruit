//
//  ContentView.swift
//  Grapefruit WatchKit Extension
//
//  Created by Михаил on 31.01.2020.
//  Copyright © 2020 Михаил. All rights reserved.
//

import SwiftUI
import UIKit

struct ContentView: View {
    var body: some View {
        TechniquesList(model: TechniquesModel())
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}

//struct CustomController : WKInterfaceObjectRepresentable {
//    typealias WKInterfaceObjectType =
//
//    func makeWKInterfaceObject(context: WKInterfaceObjectRepresentableContext<CustomController>) -> CustomController.WKInterfaceObjectType {
//
//        let storyboard = UIStoryboard(name: "Interface", bundle: Bundle.main)
//        let controller = storyboard.
//    }
//}
