//
//  HostingController.swift
//  Grapefruit WatchKit Extension
//
//  Created by Михаил on 31.01.2020.
//  Copyright © 2020 Михаил. All rights reserved.
//

import WatchKit
import Foundation
import SwiftUI

class HostingController: WKHostingController<ContentView> {
    override var body: ContentView {
        return ContentView()
    }
}

class AnimationController: WKHostingController<Animation_Screen> {
    override var body: Animation_Screen {
        return Animation_Screen()
    }
}

class SSController: WKHostingController<Start_Stop_Screen> {
    override var body: Start_Stop_Screen {
        return Start_Stop_Screen()
    }
}
